from copy import deepcopy

number = input("How many columns would you like in your game? ")
y = number
game = [list(" ") for i in range(0, y)]
grid = game * y
if len(grid) % 2 == 0:
    print("There is no middle square in this game.")

rows = len(grid) / int(y)
row_len = len(grid) / len(grid[:y])
matrix = []


def print_grid(grid_copy):
    grid_copy = deepcopy(grid_copy)
    count = 0
    row = list()
    for i in grid_copy:
        if i is list and count <= y:
            count += 1
            row.append(i)
            grid_copy.pop(0)
        elif i is str and count <= y:
            count += 1
            row.append(i)
            grid_copy.pop(0)
        elif count == y:
            matrix.append(row)
            count = 0
            row = list()

    print(matrix)


one_moves = list()
two_moves = list()
print("The Grid Squares Count From Zero")

while True:
    try:
        one = input("Player One Enter A Move As A Square's Number, You Are X: ")
        one_moves.append(one)
        one = int(one) - 1
        grid[one] = 1
        game = deepcopy(grid)
        print(game)
        two = input("Player Two Enter A Move As A Square's Number, You are O: ")
        two_moves.append(two)
        two = int(two) - 1
        grid[two] = 0
        game = deepcopy(grid)
        print(game)
    except KeyboardInterrupt:
        break



#up_line = "|"
#cross_line = "_"
#grid = cross_line * 2 + up_line + cross_line * 3 + up_line + cross_line * 2
#grid_end = "  " + up_line + "   " + up_line

#print(grid)
#print (grid)
#print (grid_end)




